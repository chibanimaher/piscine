﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Opera;
using OpenQA.Selenium.PhantomJS;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace Piscine
{
    class FixturesWEB
    {
        public IWebDriver _driver;
        public WebDriverWait _wait;
        string navigateur ="chrome";
        [SetUp]
        public void SetUp()
        {
            // Lancer le Nav
            ChooseDriverInstance(navigateur);
            // Plein Ecran
            _driver.Manage().Window.Maximize();
            // Wait pour les elements
            _wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(20));
            // 
            IJavaScriptExecutor js = (IJavaScriptExecutor)_driver;
            //((IJavaScriptExecutor)_driver).ExecuteScript(String.Format("window.scrollBy({0}, {1})", 0, "window.innerHeight"));
        }
        
        [TearDown]
        public void TearDown()
        {
            // Fermer le navigateur
            _driver.Close();
            // kill driver
            DriverKiller(navigateur);
        }

        #region Gestion instances drivers
        private void ChooseDriverInstance(string browserType)
        {
            if (browserType.ToLower() == "chrome")
            {
                _driver = new ChromeDriver(@"C:\WEBDRIVERS");
                _driver.Manage().Window.Maximize();
            }
            else if (browserType.ToLower() == "firefox")
            {
                FirefoxDriverService service = FirefoxDriverService.CreateDefaultService();
                service.FirefoxBinaryPath = @"C:\WEBDRIVERS";
                service.HideCommandPromptWindow = true;
                service.SuppressInitialDiagnosticInformation = true;
                _driver = new FirefoxDriver(@"C:\WEBDRIVERS"/*service*/);
                _driver.Manage().Window.Maximize();
            }
            else if (browserType.ToLower() == "ie")
            {
                _driver = new InternetExplorerDriver(@"C:\WEBDRIVERS");
                _driver.Manage().Window.Maximize();
            }
            else if (browserType.ToLower() == "edge")
            {
                _driver = new EdgeDriver(@"C:\WEBDRIVERS");
                _driver.Manage().Window.Maximize();
            }
            else if (browserType.ToLower() == "opera")
            {
                _driver = new OperaDriver(@"C:\WEBDRIVERS");
                _driver.Manage().Window.Maximize();
            }
            else if (browserType.ToLower() == "phantomjs")
            {
                _driver = new PhantomJSDriver(@"C:\WEBDRIVERS");
                _driver.Manage().Window.Maximize();
            }
        }
        [PermissionSetAttribute(SecurityAction.LinkDemand, Name = "FullTrust")]
        public void DriverKiller(string browserType)
        {
            Process[] listProcessDriver;
            switch (browserType)
            {
                case "chrome":
                    {
                        listProcessDriver = Process.GetProcessesByName("chromedriver");
                        foreach (var _process in listProcessDriver)
                        {
                            _process.Kill();
                        }
                    }
                    break;
                case "firefox":
                    {
                        listProcessDriver = Process.GetProcessesByName("geckodriver");
                        foreach (var _process in listProcessDriver)
                        {
                            _process.Kill();
                        }
                    }
                    break;
                case "ie":
                    break;
                case "edge":
                    {
                        listProcessDriver = Process.GetProcessesByName("MicrosoftWebDriver");
                        foreach (var _process in listProcessDriver)
                        {
                            _process.Kill();
                        }
                    }
                    break;
                case "opera":
                    {
                        listProcessDriver = Process.GetProcessesByName("operadriver");
                        foreach (var _process in listProcessDriver)
                        {
                            _process.Kill();
                        }
                    }
                    break;
                case "phantomjs":
                    {
                        listProcessDriver = Process.GetProcessesByName("phantomjs");
                        foreach (var _process in listProcessDriver)
                        {
                            _process.Kill();
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        #endregion
    }
}
