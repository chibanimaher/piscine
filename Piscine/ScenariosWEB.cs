﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Piscine
{
    class ScenariosWEB: FixturesWEB
    {
        [Test, Order(2)]
        [TestCase(TestName = "cdiscount")]
        public void TestLaFayette()
        {
            // visiter la page testing
            _driver.Navigate().GoToUrl("https://www.cdiscount.com/");
            _driver.Navigate().Refresh();

            // acceder informatique
            _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#olMain > nav > ul > li:nth-child(7) > a > span"))).Click();
            
            _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#menuV > div.mvMenu.testIntertitle > nav > div > div.mvNavLk > div > ul > li:nth-child(2) > a"))).Click();
            // choisir un lit en bas de la page 
            IWebElement lit = _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#MainZone1 > div:nth-child(18) > div.carouAnimco > div > ul > li:nth-child(2) > a > img")));
            while (!lit.Displayed)
            {
                ((IJavaScriptExecutor)_driver).ExecuteScript(String.Format("window.scrollBy({0}, {1})", 0, "window.innerHeight"));

            }
            lit.Click();
            IList<IWebElement> oRadioButton = _driver.FindElements(By.CssSelector("#fpGarserZone > form > ul > li:nth-child(1) > div:nth-child(5)"));
            bool bValue = false;

            // This statement will return True, in case of first Radio button is selected
            bValue = oRadioButton.ElementAt(0).Selected;

            // This will check that if the bValue is True means if the first radio button is selected
            if (bValue == true)
            {
                // This will select Second radio button, if the first radio button is selected by default
                oRadioButton.ElementAt(1).Click();
            }
            else
            {
                // If the first radio button is not selected by default, the first will be selected
                oRadioButton.ElementAt(0).Click();
            }

            _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#fpAddBsk"))).Click();
            _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#assGar > div:nth-child(2) > div.agClose.jsClose"))).Click();
            _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#assGar > div:nth-child(1) > div.agClose.jsClose"))).Click();
            _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#goBasket1"))).Click();
            _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#hFull > div.hLogo > a > img"))).Click();
            _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#hConnect > a > div > span"))).Click();
            _wait.Until(ExpectedConditions.ElementExists(By.Id("LoginCreationViewData_CustomerLoginCreationFormData_Email"))).SendKeys("chibanimaher@gmail.com");
            _wait.Until(ExpectedConditions.ElementExists(By.Id("newPassword"))).SendKeys("Ar@4154255258");
            _wait.Until(ExpectedConditions.ElementExists(By.Id("LoginCreationViewData_CustomerLoginCreationFormData_RePassword"))).SendKeys("Ar@4154255258");
            _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#loginCreationForm > fieldset > input.sbt.mainButton"))).Click();
           



            /* _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#fpGarserZone > form > ul > li:nth-child(1) > div:nth-child(5)"))).Click();
             _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#pdt-cell-48843180 > div.visual.opaque-visual.js-recommendation-event > a > div"))).Click();
             IReadOnlyList<IWebElement> elements = _driver.FindElements(By.ClassName("opaque"));
             //elements.FirstOrDefault().Click();
             _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#menuV > div.mvMenu.testIntertitle > nav > div > div.mvNavLk > div > ul > li:nth-child(2) > a"))).Click();
             _wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("LoginCreationViewData_CustomerLoginCreationFormData_Email"))).Click();
             _wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("select-size-list"))).SendKeys("41");
             IWebElement tailleChausure = _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#description > div.row.block-toolbox > div.columns.small-24.xmedium-12.large-12 > div > div > div.columns.large-3.show-for-large-up > ul > li:nth-child(5) > a > div > div")));
             while (!tailleChausure.Displayed)
             {
                 ((IJavaScriptExecutor)_driver).ExecuteScript(String.Format("window.scrollBy({0}, {1})", 0, "window.innerHeight"));

             }
             tailleChausure.Click();*/
            Thread.Sleep(5000);
        }
    }
}
